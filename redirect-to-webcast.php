<?php
/*
Plugin Name: Redirect To Webcast
Version: 1.1
Description: Redirects user to webcast page after successful login.
Author: Elvis Morales
Author URI: http://elvismdev.io
Plugin URI: https://bitbucket.org/grantcardone/redirect-to-webcast
Text Domain: redirect-to-webcast
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Redirect user to video page after successful login.
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */
add_filter( 'login_redirect', 'rtw_video_page_redirect', 10, 3 );
function rtw_video_page_redirect( $redirect_to, $request, $user ) {
	// die('aaaaa');
	//is there a user to check?
	global $user;
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return $redirect_to;
		} else {
			return home_url() . '/make-millions-in-business';
		}
	} else {
		return $redirect_to;
	}
}
